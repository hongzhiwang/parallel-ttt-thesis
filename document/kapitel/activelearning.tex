\chapter{Active Automata Learning and Parallelization} \label{cpt:activeLearning}
%TODO rewrite these paragraphs
%To introduce the \ttt\ Algorithm, the goal of which needs first to be clarified. That is, given an unknown target system (which can be described as an output function) and a teacher that can answer Membership Queries and Equivalence Queries, to infer the target system by building an automaton with finite states that behaves the same as the target system. 
%
%The \ttt\ Algorithm can be used to learn different automata. In this thesis, we focus mainly on the DFA variation and the adaption to Mealy Machines.


In this chapter, after introducing some necessary concepts and denotations, the framework that \ttt\ learning algorithm built upon will be described in section \ref{sec:framework}. In preparation for the parallelization of the \ttt\ algorithm, a distributed adaptation of membership oracle, and accordingly introduced complexity measures are presented in section \ref{sec:distributedMQ} and \ref{sec:complexityMeasure}.


\section{Preliminaries}\label{sec:preliminaries}
\subsection{Words, Alphabets, Languages}
A \textit{word} $ w $ is a sequence of \textit{symbols}. The length of $ w $, denoted $ |w| $, is the number of symbols contained in this sequence. An empty word is denoted $ \epsilon $, and $ |\epsilon|=0 $. Concatenation of two words $ w $, $ w' $, denoted $ w\cdot w' $ or just $ ww' $, is obtained by writing $ w' $ directly after $ w $.
An \textit{alphabet} $ \varSigma $ is a finite set of symbols. The set of all words over $ \varSigma $ is a \textit{language}, denoted $ \varSigma^{*} $. And we define $ \varSigma^{+} = \varSigma^{*}\setminus\{\epsilon\} $.
 \cite{hopcroft_introduction_1979}
\subsection{Deterministic Finite Automaton}
\definition[DFA]{A deterministic finite automaton $ \mathcal{A} $ is a 5-tuple $ \mathcal{A}=(\mathcal{Q}, \varSigma, \delta, q_{0}, \mathcal{F}) $, where
\begin{itemize}
	\item $ \mathcal{Q} $ is a finite set of states,
	\item $ \varSigma $ is an alphabet of input symbols,
	\item $ \delta:\mathcal{Q}\times\varSigma\rightarrow\mathcal{Q} $ is the transition function,
	\item $ q_{0} \in \mathcal{Q}$ is the initial state, and
	\item $ \mathcal{F} \subseteq \mathcal{Q} $ is the set of accepting states.
\end{itemize}
}

The transition function $ \delta:\mathcal{Q}\times\varSigma\rightarrow\mathcal{Q} $ can be extended to $ \delta^{*}:\mathcal{Q}\times\varSigma^{*}\rightarrow\mathcal{Q} $ by defining $ \delta^*(q,wa)=\delta(\delta^*(q, w),a) $ and $ \delta^*(q, \epsilon)=q $.
%TODO not finished yet, extend this if needed somewhere.

%\definition[Mealy Machine]{A mealy machine $ \mathcal{M} $ is a 6-tuple $ \mathcal{M}=(\mathcal{Q}, q_{0}, \varSigma, \varDelta, \delta, \lambda) $ where
%\begin{itemize}
%	\item $ \mathcal{Q} $ is a finite set of states,
%	\item $ q_{0} \in \mathcal{Q}$ is the initial state, and
%	\item $ \varSigma $ is an alphabet of input symbols,
%	\item $ \varDelta $ is an alphabet of output symbols,
%	\item $ \delta:\mathcal{Q}\times\varSigma\rightarrow\mathcal{Q} $ is the transition function, and
%	\item $ \lambda:\mathcal{Q}\times\varSigma\rightarrow\varDelta $ is the output function.
%\end{itemize}
%}


\section{The Active Automata Learning Framework}
\label{sec:framework}
The \ttt\ Algorithm infers the \textit{System Under Learning} (SUL) without any knowledge of its internal structure. Provided only with a black box system, the learner \textit{actively} invokes a series of queries and observes the responses to construct and verify the \textit{System Under Test} (SUT). More precisely speaking, the \ttt\ Algorithm uses an Angluin-style setup, as known as the Minimally Adequate Teacher Model.
\subsection{Minimally Adequate Teacher Model}
The \textit{Minimally Adequate Teacher} (MAT) learning model was first introduced by Angluin \cite{angluin_learning_1987}, in which the teacher can answer two sorts of questions. That is:
\begin{description}
	\item[Membership Query (MQ)] consisting of  word that the learner asks the teacher and its answer.
	\item[Equivalence Query (EQ)] consisting of a hypothesis model that the learner constructed, and the answer given by the teacher whether this is equal to the target system. If the answer is no, the counterexample is delivered as the answer.
\end{description}

In practice, the teacher is implemented by two oracles - the \textit{Membership Oracle} and the \textit{Equivalence Oracle}. ( as shown in Figure \ref{fig:matSetup}) Without any knowledge of the SUL, a throughout equivalence test is impossible to realize. Even some parameter of the SUL (e.g. an upper bound of the number of states) are given, as discussed in \cite{hutchison_introduction_2011}, the realization of the equivalence oracle is rather difficult. Aside from that, the concept of equivalence test is elegant and helpful for model construction.

\begin{figure}[th]
	\centering
	\begin{tikzpicture}[node distance=1cm, auto,]
	\tikzset{
	  	%Define standard arrow tip
	  	>=stealth',
	  	%Define style for boxes
	  	moracle/.style={ rectangle, rounded corners, draw=black, very thick, text width=6.5em, minimum height=2em, text centered},
	  	% Define arrow style
	  	pil/.style={ ->, thick, shorten <=2pt, shorten >=2pt,}
	}
	
	\node[draw=black, very thick, ellipse](hypo){Hypothesis};
	\node[above=of hypo](dummy){};
	\node[moracle, below=of hypo](mo){Membership Oracle};
	\node[moracle, left=of dummy](learner){Learner}
	edge[pil] node[auto]{}(hypo)
	edge[pil,bend right=45] node[auto, swap](mq){Membership Queries}(mo);
	\node[moracle, right=of dummy](eo){Equivalence Oracle}
	edge[pil, bend right=30] node[above, swap] {Counter Example}(learner)
	edge[pil, bend left=45] node[auto]{Membership Queries}(mo);
	\node[above left=of learner](init){}
	edge[pil] node[auto] {init} (learner);
	\path[->]
	(hypo) edge[pil] node[auto]{}(eo);
	
	\end{tikzpicture} 

	\caption{The Minimally Adequate Teacher Model}
	\label{fig:matSetup}
\end{figure}

\subsection{The Learning Loop}
Now that the MAT model has been established, the learning process is quite simple to describe. Algorithm \ref{alg:learningLoop} shows the common loop of acquiring the final Hypothesis model considered equivalent to the SUL.
\begin{algorithm}[th]
	\caption{Learning Loop}\label{alg:learningLoop}
	\begin{algorithmic}
		\Require A Membership Oracle that can answer MQs, an Equivalence Oracle that can answer EQs, the Learner
		\Ensure Final Hypothesis Model that passes EQ
		\State Learner constructs initial hypothesis using MQs
		\Loop
		\State Let $\mathcal{H}$ be the hypothesis from learner
		\If{EQ($\mathcal{H}$) returns Counterexample $ce$}
		\State Learner refines Hypothesis Model with $ce$ and MQs
		\Else
		\State \textbf{return} $\mathcal{H}$
		\EndIf
		\EndLoop
		
	\end{algorithmic}
	
\end{algorithm}

After the initial construction, the hypothesis will be challenged in each loop of learning. Depending on the result of Equivalence Test, the hypothesis will either be accepted as the final learning result, or be further refined by the learner to include the information carried by the counterexample.

\section{The Distributed Membership Oracle}
\label{sec:distributedMQ}
For the purpose of parallelization, a distributed version of membership oracle is introduced in \cite{howar_teachers_2012} as \textit{the teachers' crowd}. 


	\begin{figure}[h]
		\tikzstyle{oracle}=[draw, very thick, rounded corners, text width=10em, text centered, minimum height=1.8em]
		\tikzstyle{dot}=[text width=5em, text centered, minimum height=2.5em]
		\tikzstyle{moracle}=[draw, very thick, rounded corners, text width=6em, text centered, minimum height=2.5em]
		\centering
		\begin{tikzpicture}[node distance=3cm, auto,>=latex', thick]
		\node[oracle] at (0,4.5)(ttt){ Learner};
		\path[->] node[oracle] at (0,3)(cacheOracle){ Cache Oracle}
		(ttt) edge node {MQs in a batch} (cacheOracle);
		\path[->] node[oracle] at (0,1.5) (parallelOracle){ Parallel Oracle}
		(cacheOracle) edge node {(Reduced) MQs in a batch} (parallelOracle);
		\path[->] node[moracle] at (-3.3,0) (mOracle1){ Membership Oracle}
		(parallelOracle) edge node {}(mOracle1);
		\path[->] node[moracle] at (-0.55,0) (mOracle2){ Membership Oracle}
		(parallelOracle) edge node {}(mOracle2);
		\path[->] node[dot] at (1.365,0) (mOracledot){ \dots};
		
		\path[->] node[moracle] at (3.3,0) (mOracle3){ Membership Oracle}
		(parallelOracle) edge node {}(mOracle3);
		
		\node[] at (-2.4,0.8) {MQs};
		\node[] at (0.2,0.8) {MQs};
		\node[] at (2.4,0.8) {MQs};
		
		%\node[text width=16em, text centered] at (2.2,3.75) {Batch of MQs};
		%\node[text width=16em, text centered] at (2.2,2.25) {(Reduced) Batch of MQs};
		\end{tikzpicture}
		\caption{Parallel oracle configuration}\label{fig:paralleloracle}
	\end{figure}

Figure \ref{fig:paralleloracle} shows an example of parallel oracle. Instead of processing single membership queries, the membership oracle now receives a batch of membership queries, and distributes them to the workers, which each holds an SUL instance, and can answer the queries simultaneously. Normally, a cache oracle is deployed between the learner and the actual membership oracle to eliminate duplicated queries.

\section{Evaluation of Learners}
\label{sec:complexityMeasure}
Traditionally, an evaluation of a learner algorithm includes following complexity measures:
\begin{description}
	\item[Membership Query Complexity] the number of membership queries that sent by the learner,
	\item[Equivalence Query Complexity] the number of equivalence queries that invoked in the learning loop, and
	\item[Symbol Complexity] the sum of the number of symbols in each word from membership queries.
\end{description}

Since the goal of parallelization of the \ttt\ Algorithm is to accomplish the same logic more efficiently. The result of refining a same counterexample would be unchanged. Thus the equivalence query complexity should not differ after parallelization. Several strategies of parallelization will be realized in Chapter \ref{cpt:parallelization}, of which there are redundant-free strategies and also those that attempt to increase parallel efficiency by invoking redundant queries. For those not redundant-free strategies, the membership query and the symbol complexity are appropriate controlling indicators.

With the distributed membership oracle setup, some new measures are to be added:
\subsubsection{Batch Complexity}
The batch complexity is the number of membership query batches that sent by learner during the learning process.

Considering that the number of queries a batch can contain varies in a fairly wide range, this measure is not an as significant description of the execution time cost as the following ones.
\subsubsection{Parallel Membership Query Complexity}
When a batch of queries are distributed to several workers, each worker can receive different number of queries. In a scenario that the time cost resetting a testing system is so essential that the execution time for symbols does not influence the total time cost much, every query has a similar time cost. Thus the largest number of queries sent to one worker can be counted as the parallel membership query cost for this batch. And the cost for whole learning process can be obtained by simply accumulating these numbers over all the batches.

\subsubsection{Parallel Symbol Query Complexity}
In a scenario that each symbol cost similar time to execute, and the fixed overhead is relatively short, the time cost of a batch depends on the worker that executes the longest symbol sequence. 
Therefore, this measure takes the maximum symbol count within a batch by one worker for the parallel symbol query cost of a batch, and accumulates these over all batches to get the total parallel symbol query cost of the learning process.

\subsubsection{Speedup}
As this thesis focuses on the parallelization of \ttt\ learning algorithm, the parallel query and symbol costs of the parallelized algorithm will be compared to that of the original one. The \textit{Query Speedup} \cite{gupta_performance_1993} can be given by $ \frac{Q_1}{Q_P} $, where $ Q_1 $ is the sequential query cost, and $ Q_P $ the parallel query cost. In an analogical manner, the \textit{Symbol Speedup} is defined by $ \frac{S_1}{S_P} $, where $ S_1 $ is the sequential symbol cost, and $ S_P $ the parallel symbol cost. 
