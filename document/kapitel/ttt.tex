\chapter{The \ttt\ Algorithm}\label{cpt:ttt}

The \ttt\ algorithm is presented by Isberner \textit{et al}. \cite{isberner_ttt_2014, isberner_foundations_2015}, which is a time and space efficient algorithm for actively inferring automata. \ttt\ is a learning algorithm with discrimination tree. The key feature of the \ttt\ is the elimination of redundant information in counterexamples by means of actively analyzing its internal data. % It optimizes the \textit{Observation Pack} algorithm \cite{howar_active_2012} to eliminate redundant information in counterexamples by means of actively analyzing its internal data.

This chapter focuses on introducing and analyzing the \ttt\ Algorithm, and searches for the interesting spots that potentially can be optimized with the distributed oracle.



\section{Data Structure}
%The name \textit{TTT} stands for following terms:
%\begin{description}
%	\item[Spanning \underline{T}ree] The spanning tree is composed of all states of the hypothesis and their definite transitions that indicate the access sequences.
%	\item[Discrimination \underline{T}ree] The discrimination tree is a decision tree that deducts which state a specific access sequence leading to. Each inner node contains a discriminator, and each leaf node is associated with a state.
%	\item[Discriminator \underline{T}rie] The discriminators in the discrimination tree can always be reorganized to a trie, so that the label of every node agrees with the path to the root.
%\end{description}

\begin{figure}[th]
	\centering
	\begin{tikzpicture}
	\begin{scope}[shorten >=0pt,node distance=2cm,on grid,auto,local bounding box=hypo, scale=0.8,transform shape]
	\node[state,initial] (q_0)   {$q_0$}; 
	\node[state] (q_1) [right=of q_0] {$q_1$}; 
	\node[state] (q_2) [right=of q_1] {$q_2$};
	\node[state] (q_3) [above=of q_0] {$q_3$};
	\node[state] (q_4) [above=of q_1] {$q_4$};
	\node[state,accepting] (q_5) [above=of q_2] {$q_5$};
	\path[->]
	(q_0) edge [line width=1.3pt] node {a} (q_1)
	edge [bend left=25,line width=1.3pt] node {b} (q_3)
	(q_1) edge [line width=1.3pt] node {a} (q_2)
		edge [bend left=25,line width=1.3pt] node {b} (q_4)
	(q_2) edge [loop right] node {a} (q_3)
	edge [bend left=25] node {b} (q_5)
	(q_3) edge [] node {a} (q_4)
	edge [bend left=25] node {b} (q_0)
	(q_4) edge [line width=1.3pt] node {a} (q_5)
	edge [bend left=25] node {b} (q_1)
	(q_5) edge [loop right] node {a} (q_5)
	edge [bend left=25] node {b} (q_2)	
	;
	\end{scope}
	\begin{scope}[
	level/.style={sibling distance=6em},
	level 4/.style={sibling distance=3em},
	every node/.style = {align=center, text centered, minimum height=2em},
	edge from parent/.style={draw,-latex},
	local bounding box=dtree,
	shift={($(hypo.north east)+(4em,5em)$)},
	scale=0.8,transform shape
	]
	\node[dtnode](epsilon){$\epsilon$}
	child {node[dtleaf](nq5){$q_5$} edge from parent node[left,near start] {$\top$}}
	child {node[dtnode](na){$a$}
		child {node[dtleaf](nq4){$q_4$} edge from parent node[left, near start]{$\top$}}
		child {node[dtnode](nba){$ba$}
			child {node[dtnode](nb){$b$}
				child {node[dtleaf](nq2){$q_2$} edge from parent node[left,near start] {$\top$}}
				child {node[dtleaf](nq1){$q_1$} edge from parent node[right,near start] {$\bot$}}
				edge from parent node[left, near start]{$\top$}}
			child {node[dtnode](naa){$aa$}
				child {node[dtleaf](nq3){$q_3$} edge from parent node[left,near start] {$\top$}}
				child {node[dtleaf](nq9){$q_0$} edge from parent node[right,near start] {$\bot$}}
				edge from parent node[right, near start]{$\bot$}}
			edge from parent node[right, near start]{$\bot$}}
		edge from parent node[right,near start] {$\bot$}
	}
	;
		\path[-]
		(q_1) edge [dashed, bend right=55] (nq1)
		(q_2) edge [dashed, bend right=35](nq2)
		(q_5) edge [dashed, bend left=60](nq5)
		;
	\end{scope}
	\begin{scope}[
	level/.style={sibling distance=6em},
	level 4/.style={sibling distance=3em},
	every node/.style = {align=center, text centered, minimum height=2em},
	edge from parent/.style={draw,latex-},
	local bounding box=trie,
	shift={($(dtree.east)+(8em,5em)$)},
	scale=0.8,transform shape
	]
	\node[dtnode](teps){$\epsilon$}
	child {node[dtnode](ta){$a$} 
		child {node[dtnode](taa){$aa$} edge from parent node[left, near start]{$a$}}
		child {node[dtnode](tba){$ba$} edge from parent node[right,near start] {$b$}}
		edge from parent node[left,near start] {$a$}}
	child {node[dtnode](tb){$b$}
		edge from parent node[right,near start] {$b$}
	}
	;
	\path[-]
	(teps) edge [dashed, bend right=25] (epsilon)
	(ta) edge [dashed, bend right=35](na)
	(taa) edge [dashed, bend right=45](naa)
	;
	\end{scope}
	\end{tikzpicture}
	\caption{Data structure of \ttt\ algorithm. Left to right: hypothesis spanning-tree, discrimination tree, suffix-trie. (source:  \cite{howar_tutorial:_2014})}
	\label{fig:ttt}
\end{figure}
%TODO change figure

\subsection{Hypothesis}
The Hypothesis contains \textit{States} and \textit{Transitions}. The states are so organized in a \textit{Spanning Tree}, that each state (except the initial state) is defined by its parent edge, which is named \textit{tree transition}. Thus the path from root to one state defines the access sequence of it.

Besides tree transitions, the so-called non-tree transitions point to leaf nodes in the \textit{Discrimination Tree}.

Every transition that pointing at the same target state provides a equivalent access sequence, which will be replaced with a unique one that defined by the spanning tree, denoted $ \lfloor\cdot\rfloor_{\mathcal{H}} $ here.

\subsection{Discrimination Tree}\label{sec:discriminationTree}
The Discrimination Tree is composed of inner nodes, which each contains a discriminator, and leaf nodes, which each corresponds to a hypothesis state.

The discrimination tree classifies an access sequence to a leaf node, i.e. a hypothesis state. This can be achieved by invoking a \textit{SIFT} procedure \cite{kearns_introduction_1994}. As shown in Algorithm \ref{alg:sift}, the \textit{SIFT} procedure uses the discrimination tree as a decision tree. At each inner node, a child node is chosen according to the response of querying the access sequence concatenated by the discriminator.

\begin{algorithm}[th]
	\caption{SIFT Procedure of Discrimination Tree}\label{alg:sift}
	\begin{algorithmic}
		\Require A start node $ n $ of Discrimination tree, Access sequence $ w $, and Oracle that answers MQs
		\Ensure A leaf node of Discrimination tree
		\If {$ n $ is a leaf node}
			\State \Return $ n $
		\Else
			\State Let $ d $ be the discriminator of $ n $
			\State Let $ n' $ be the child node of $ n $ with edge property $ MQ(wd) $
			\If{$ n' $ does not exist}
				\State create new child node with edge property $ MQ(wd) $
			\EndIf
			\State \textbf{return} $ SIFT( n', w ) $
		\EndIf
	\end{algorithmic}
\end{algorithm}

The discriminators in the discrimination tree correspond to a \textit{Suffix Trie}. The spanning \underline{T}ree, the discrimination \underline{T}ree and the suffix \underline{T}rie constitute the name of TTT.

\section{Key Steps of \ttt\ Algorithm}
As shown in Algorithm \ref{alg:learningLoop}, the \ttt\ algorithm constructs first a simple initial Hypothesis and processes it in each iteration according to the counterexample as long as the Equivalence Test is not yet passed.

\begin{algorithm}[th]
	\caption{\ttt\ Refinement}\label{alg:refine}
	\begin{algorithmic}
		\Require Counterexample $ ce $, \ttt\ Learner
		\Ensure Learner with refined Hypothesis
		\While{$ \mathcal{H}(ce) $ is equal to $ MQ(ce) $}
		\State $ ce_{current} \leftarrow ce$ 
		\Repeat
		\State $ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}}(ce_{current}) $
		\State $ \textit{\textsc{Finalization}} $
		\State $ ce_{current} \leftarrow \textsc{\textit{FindInconsistency}}$ 
		\Until{$ ce_{current} $ is $ Null $ }
		\EndWhile
	\end{algorithmic}
\end{algorithm}

Algorithm \ref{alg:refine} describes more detail of this generalized refinement process. To be more precise, the procedure that actually performs the refinement is called  $ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}} $  in the \ttt\ tlgorithm. After each $ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}} $ procedure, 
\ttt\ optimizes all the new discriminators in its \textit{\textsc{Finalization}} step. In the stabilization step, a consistency check that ensures the hypothesis has the same output as the discrimination tree is carried out. Any inconsistency will be treated as a new counterexample.

\subsection{Hypothesis Construction}
The initial construction of hypothesis is rather simple. After construction, \ttt\ has a discrimination tree initialized with a root node, which has a discriminator $ \epsilon $ and no child node. Note that the root node must have at least one child as an automaton without state is logically impossible. 
The construction of hypothesis begins with a initial state, whose access sequence is defined as $ \epsilon $. 
This state is then associated with a node that created in a \textit{SIFT} procedure. 
Subsequently the state is initialized.

\subsubsection{Hypothesis State Initialization}\label{sec:stateInit}

A state is initialized by adding transitions coming out of it that correspond to every possible input symbol. These transitions are \textit{open} as the destinations are not yet determined, in the \ttt\ algorithm, they are handled by pointing to the root node of the discrimination tree. All open transitions are maintained in a list.

In the DFA version of \ttt
, the accepting property, or whether this state $ s \in \mathcal{F} $, is assigned according to the parent edge of the associated leaf node.

% In the Mealy Machine version of \ttt\, every transition has a property that reflects the output function. Thus by constructing each transition, a membership query is required, which indicating demanding for parallelization.


\subsubsection{Closing Transitions}\label{sec:closeTransitions}

The last step of hypothesis initialization is closing transitions (Algorithm \ref{alg:closeTransitions}). 

\begin{algorithm}[th]
	\caption{Close Transitions}\label{alg:closeTransitions}
	\begin{algorithmic}
			
		\Require List of Open Transitions $ \mathcal{L} $, \ttt\ Learner
		\Ensure Learner with all transitions closed
		\While{$ \mathcal{L} $ is not empty}
			\State Transition $ t \leftarrow \mathcal{L}.pop()$ 
			\State Node $ n \leftarrow SIFT(t.targetNode, t.accessSequence) $
			\If{$ n $ has no associated state}
				\State Create State $ s $ according to $ t $
				\State Associate $ n $ with $ s $
				\State Initialize $ s $
			\EndIf
			\State Target $ t $ to State associated with $ n $
		\EndWhile

	\end{algorithmic}
\end{algorithm}

In this procedure, every open transition, with its access sequence, will be classified to a leaf node in the discrimination tree. In other word, it will be assigned with a destination state that associated with its SIFT result.
With all transitions closed, the hypothesis is now initialized as a complete Automaton pending the first equivalence test.

To be noticed that this procedure is also an essential finishing step in refinement process. And concerning parallelization, SIFT procedure, which is a primary source of membership queries, is repeatedly invoked here. That is to say, this procedure can potentially be pa\-ra\-lle\-li\-zed. The detail of parallelization is presented in Chapter \ref{cpt:normalParallelization} and \ref{cpt:mod_parallelization}.

\subsection{Hypothesis Refinement ($ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}} $)} \label{sec:refine_single}
In case of an unsuccessful equivalence test, a counterexample is raised, with which the learner can further refine the hypothesis accordingly. The key of this step is to determine the very symbol in the counterexample, that leads to different states in SUL and hypothesis. Which indicates that at least two states in SUL are incorrectly classified to one state in hypothesis. And to refine the hypothesis, this state must be split.

The procedure that identifies the splitting symbol in counterexample is called word \textit{Decomposition}.  %TODO \cite{Rivest & Schapire}
The word $ w $ from the counterexample can be decomposed as $ w = u\cdot a\cdot v $ ($ u,v \in \varSigma^{*}, a \in \varSigma $), so that $ u, a, v $ satisfy $ \lambda_{SUL}(\lfloor u \rfloor _{\mathcal{H}} \cdot a \cdot v) \neq \lambda_{SUL}(\lfloor u \cdot a \rfloor_{\mathcal{H}} \cdot v)$. Searching for $ a $ is not complicated. Given the observation that for any cut of $ w = p \cdot q $, either $ \lambda_{SUL} ( \lfloor p \rfloor _{\mathcal{H}} \cdot q ) = \lambda_{SUL} (w) $, in which case $ a \in q $, or $ \lambda_{SUL} ( \lfloor p \rfloor _{\mathcal{H}} \cdot q ) \neq \lambda_{SUL} (w) $, in which case $ a \in p $, we can use linear or binary search to find this splitting symbol.

Regarding the perspective of parallelization, in worst case scenario, the decomposition need $ \lceil log_{2}(|w|) \rceil $ (with binary search) membership queries. As those queries are not independent, they can be parallelized only after the modification of the decomposition algorithm in Section \ref{sec:paralleldecomposition}. 

Once the decomposition is done. A new state with access sequence $ \lfloor u \rfloor _{\mathcal{H}} \cdot a $ needs to be split from the old state with $ \lfloor u \cdot a \rfloor _{\mathcal{H}}$. Correspondingly, the node associated with this old state needs to be split with the discriminator $ v $, and a new node associated with the new state must be created.

Exactly two membership queries are invoked here, to determine the edge property of the child nodes of the new discriminator. As this information is probably already contained in the discrimination tree, the \ttt\ tries to answer them first by deriding with known information and only invokes queries when fails.
%TODO not exactly!

This new discriminator is marked as temporary, that defines the subtree with this as root node a temporary block. Which means a finalization operation is still required in order to eliminate redundant symbols in the discriminators.

\subsection{Discriminator Finalization}

\subsubsection{Soft Closing of Transitions}
After $ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}} $, there may be open transitions pointing to the root of the discrimination tree. As it is sufficient for the use of finalization that the transitions are only SIFTed down to a temporary block root, a so-called \textit{soft SIFT} is introduced here.

The difference of soft SIFT to its original version is that every temporary inner node is treated as a leaf node. As the queried words only involve finalized discriminators, the symbol complexity is optimized.

%\ttt\ closes a transition only when it is used. \footnote{In the newest version of \ttt (since the commit on September 17th, 2015), this is already changed.} 
%A few modifications can be done for the parallelization, this will be presented in \ref{sec:collectingSifts}.

\subsubsection{Finding Splitter}
Finalizing a temporary block is accomplished with replacement of a new discriminator, which is found in following situation: $ q $, $ q' $ are two states with associated leaf nodes in the same temporary block. Such an $ a \in \varSigma $ can be found that the transitions with a for $ q $ and $ q' $ point to two nodes in different blocks. In this situation, the concatenation of the discriminator in the least common ancestor node and $ a $ provides a new discriminator. From the implementing point of view, such a least common ancestor and $ a $ constitutes a \textit{Splitter}. An example is shown in Figure \ref{fig:findingSplitter}. $ q_{1} $ and $ q_{3} $ are in the same temporary block (\ref{fig:findingSplitter_1}). A $ b $-transition leads to node $ q_{0} $ and block root $ n_{ab} $ respectively, which are not in the same block. The least common ancestor of $ q_{0} $ and $ n_{ab} $ is the root node of discrimination tree (\ref{fig:findingSplitter_2}). In other word, after a $ b $-transition, state $ q_{1} $ and state $ q_{3} $ can be discriminated by the discriminator $ \epsilon $. It is not difficult to be seen that $ b \cdot \epsilon $ constitutes a new discriminator for the two states (\ref{fig:findingSplitter_3}).


\begin{figure}[htb]
	
	\begin{subfigure}[b]{.48\linewidth} 
		\centering
		\resizebox{.7\linewidth}{!}{ %
			\begin{tikzpicture}
			
			\begin{scope}[shorten >=0pt,node distance=2cm,on grid,auto,local bounding box=hypo, scale=1,transform shape]
			\node[state,initial,accepting] (q_0)   {$q_0$}; 
			\node[state] (q_1) [below=of q_0] {$q_1$}; 
			\node[state] (q_2) [right=of q_0] {$q_2$};
			\node[state] (q_3) [below=of q_2] {$q_3$};
			\path[->]
			(q_0) edge [bend left=15] node {a} (q_2)
			edge [bend left=15] node {b} (q_1)
			(q_1) edge [bend left=15] node {b} (q_0)
			(q_2) edge [bend left=15] node {a} (q_3)
			edge [loop above] node {b} (q_2)
			
			;
			\end{scope}
			\begin{scope}[
			level/.style={sibling distance=6em},
			every node/.style = {align=center, text centered, minimum height=2em},
			edge from parent/.style={draw,-latex},
			local bounding box=dtree,
			shift={($(hypo.south)+(0em,-2em)$)},
			scale=1,transform shape
			]
			\node[dtnode](epsilon){$\epsilon$}
			child {node[dtleaf](l0){$q_0$} edge from parent node[left,near start] {$\top$}}
			child {node[dtnode_temp](lab){$ab$}
				child {node[dtleaf](l1){$q_1$} edge from parent node[left, near start]{$\top$}}
				child {node[dtnode_temp](lb){$b$}
					child {node[dtleaf](l2){$q_2$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](l3){$q_3$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[right,near start] {$\bot$}
			}
			;
			\path[->]
			(q_1) edge [dashed, bend right=55] node[auto, near start, swap]{a} (lab)
			(q_3) edge [dashed, bend left=15] node[auto, near start, swap]{a} (lab)
			(q_3) edge [dashed, bend left=60] node[auto, near start]{b} (lab)
			;
			
			\end{scope}
			\begin{pgfonlayer}{background}
			\begin{scope}
			\node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(lab) (l1) (l2) (lb) (l3)](fit1){};
			\end{scope}
			\end{pgfonlayer}
			\end{tikzpicture} %
		}
		\caption{hypothesis and discrimination tree with temporary block after soft closing transitions} \label{fig:findingSplitter_1}
		
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.48\linewidth}
		\centering
		\resizebox{\linewidth}{!}{ %
			
			\begin{tikzpicture}
			
			\begin{scope}[
			level/.style={sibling distance=6em},
			every node/.style = {align=center, text centered, minimum height=2em},
			shift={($(hypo.east)+(12em,1em)$)},
			scale=1,
			transform shape
			]
			\node[dtnode_temp](t_ab){$ab$}
			child{node[dtleaf](l_q1){$q_1$} [dashed] edge from parent node[left, near start]{$\top$}}
			child{node[dtleaf](l_q3){$q_3$} [dashed] edge from parent node[right, near start]{$\bot$}}
			;
			\node[dtnode, right=12em of t_ab](t_eps){$\epsilon$}
			child{node[dtleaf](r_q0){$q_0$} [dashed] edge from parent node[left, near start]{$\top$}}
			child{node[dtnode_temp](r_ab){$ab$} [dashed] edge from parent node[right, near start]{$\bot$}}
			;
			\path[->]
			(l_q1) edge [dashed, bend right=45] node [auto, near end] {$b$} (r_q0)
			(l_q3) edge [dashed, bend right=45] node [auto, near end] {$b$} (r_ab)
			;
			\end{scope} %
			
			\end{tikzpicture} %
		}
		\caption{finding a new discriminator $ b|\epsilon $ for $ q_{0} $ and $ q_{1} $} \label{fig:findingSplitter_2}
		\vspace{1.2 cm}
		\resizebox{0.4\linewidth}{!}{ %
			\begin{tikzpicture} %
			
			\begin{scope}[ %
			level/.style={sibling distance=6em},
			every node/.style = {align=center, text centered, minimum height=2em},
			shift={($(hypo.east)+(12em,1em)$)},
			scale=1,
			transform shape
			]
			\node[dtnode]{$b|\epsilon$}
			child{node[dtleaf]{$q_1$} [dashed] edge from parent node[left, near start]{$\top$}}
			child{node[dtleaf]{$q_3$} [dashed] edge from parent node[right, near start]{$\bot$}}
			;
			\end{scope} %
			\end{tikzpicture} %
		}
		\caption{expected partial discrimination tree after splitting} \label{fig:findingSplitter_3}
	\end{subfigure} %
	~
	\begin{subfigure}[t]{0.15\linewidth}
		
		
	\end{subfigure}
	\caption{An example of finding a splitter} \label{fig:findingSplitter}
\end{figure}

In search of splitters in a certain block, every possible pair of leaf nodes are inspected with every $ a \in \varSigma $. After searching all blocks, there may be more than one splitters found. In this case, the splitter with shortest discriminator is chosen.

\subsubsection{Marking Block}
With a splitter found, the block is subsequently to be split according to the response to the new discriminator. The splitting can be further divided into two steps: \textit{Marking} and \textit{Extracting}.

The strategy of marking is to mark each leaf node and each transition to inner node with the output symbol given by querying the concatenation of its access sequence and the new discriminator. And the same mark spreads upwards to its every ancestor in this block. For the case of transitions, the targeted node is marked with its output as well. 

Let us explain this further with the same example in Figure \ref{fig:marking_extract_1}. $ q_2 $ has an access sequence $ a $, and $ \lambda_{SUL}(a\cdot b)=\top $. Thus $ q_2 $, $ n_b $ and $ n_{ab} $ are marked as $ \top $. $ q_3 $ has an access sequence $ aa $, and $ \lambda_{SUL}(aa \cdot b) = \bot $. Thus $ q_3 $, $ n_b $, $ n_{ab} $ are marked as $ \bot $. Now the two inner nodes are marked as both $ \top $ and $ \bot $. We leave the marking of the rest out to keep the conciseness. Note that most of the time output of this query can be deducted by observing the discrimination tree. We take again $ q_2 $ for example. In Figure \ref{fig:findingSplitter_1}, the target of $ a $-transition from $ q_2 $ is $ q_2 $ itself, and $ q_2 $ is in the $ \top $-subtree of discriminator $ b $. Hence the output is deducted as $ \top $. The marking for transitions, on the other hand, needs always membership queries. 

Regarding the perspective of parallelization, every incoming transition of inner node requires a membership query. These queries are independent, thus can be directly parallelized.


\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{0.27\textwidth}
		\resizebox{\textwidth}{!}{%
  \begin{tikzpicture}
  \tikzstyle{accepted}=[fill=gray!10, text=black]
  \tikzstyle{rejected}=[fill=gray!80, text=white]
  \tikzstyle{both}=[fill=gray!45, text=black]
  
  \begin{scope}[
  level/.style={sibling distance=6em},
  every node/.style = {align=center, text centered, minimum height=2em},
  edge from parent/.style={draw,-latex},
  local bounding box=dtree,
  scale=1,transform shape
  ]
  \node[both, dtnode_temp](lab){$ab$}
  child {node[accepted, dtleaf](l1){$q_1$} edge from parent node[left, near start]{$\top$}}
  child {node[both, dtnode_temp](lb){$b$}
  	child {node[accepted, dtleaf](l2){$q_2$} edge from parent node[left, near start]{$\top$}}
  	child {node[rejected, dtleaf](l3){$q_3$} edge from parent node[right, near start]{$\bot$}}
  	edge from parent node[right, near start]{$\bot$}}
  ;
  \node[below=0em of l1]{$b$};
  \node[ below=0em of l2]{$a$};
  \node[below=0em of l3]{$aa$};
  \node[circle, dashed, draw, above left=1em and 3em of lab](q1a){$q_1$};
  \node[circle, dashed, draw, above right=1em and 3em of lab](q3a){$q_3$};
  %       \node[circle, dashed, draw, below left=1em and 3em of lab](q3b){$q_3$};
  
  \path[->]
  (q1a) edge[bend left=30]  node [auto, inner sep=0pt, near start] {$a (\top)$} (lab)
  (q3a) edge[bend left=30] node [auto, inner sep=0pt, near start] {$a (\bot)$} (lab)
  (q3a)  edge[bend right=30] node [auto, inner sep=0pt, swap, near start] {$ b (\bot)$} (lab)
  ;
  
  \end{scope}
  
  \end{tikzpicture}%
		}
	\caption{Marking a temporary block($ q_1 $, $ q_{2} $ are marked as $ \top $; $ q_3 $ as $ \bot $; the two inner nodes are both $ \top $ and $ \bot $.)}\label{fig:marking_extract_1}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.4\textwidth}
		\centering
		\resizebox{\textwidth}{!}{ %
			  \begin{tikzpicture}
  \tikzstyle{accepted}=[fill=gray!10, text=black]
  \tikzstyle{rejected}=[fill=gray!80, text=white]
  \tikzstyle{both}=[fill=gray!45, text=black]

  \begin{scope}[
  level/.style={sibling distance=6em},
  edge from parent/.style={draw, -latex},
  local bounding box=dtree,
  scale=1,transform shape,
  every node/.style = {align=center, text centered, minimum height=2em}
  ]
  \node[dtnode]{$b$}
  child {node[dtnode_temp, both](lab1){$ab$}
		  	child {node[dtleaf, accepted]{$q_1$} edge from parent node[left, near start]{$\top$}}
		  	child {node[dtnode_temp, both]{$b$}
		  		child {node[dtleaf, accepted]{$q_2$} edge from parent node[left, near start]{$\top$}}
		  		child [missing]{node{}}
		  		edge from parent node[right, near start]{$\bot$}
		  	}
		  	edge from parent node[left, near start]{$\top$}
  }
  child{ node[dtnode_temp, both](lab2){$ab$}
		  	child[missing]{node{}}
		  	child{ node[dtnode_temp, both]{$b$}
		  		child[missing]{ node{}}
		  		child{ node[dtleaf, rejected]{$q_3$} edge from parent node[right, near start]{$\bot$}}
		  		edge from parent node[right, near start]{$\bot$}
		  	}
		  	edge from parent node[right, near start]{$\bot$}	 
  }
  ;
  \node[circle, dashed, draw, above left=1em and 3em of lab1](q1){$q_1$};
  \node[circle, dashed, draw, above right=1em and 3em of lab2](q3){$q_3$};
  \path[->]
  (q1) edge [bend left=30] node [auto, near start] {$a (\top)$} (lab1)
  (q3) edge[bend left=30]  node [auto, near start] {$a (\bot)$} (lab2)
  (q3)  edge[ bend right=30] node [auto, near start, swap] {$b (\bot)$} (lab2)
  ;
  \end{scope}
  

  \end{tikzpicture} %

		}
	\caption{Direct result of extracting subtrees}\label{fig:marking_extract_2} %
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.25\textwidth}
	\resizebox{\textwidth}{!}{ %
	  \begin{tikzpicture}
	  \pgfdeclarelayer{background}

	  \pgfsetlayers{background,main}


	  \begin{scope}[
	  level/.style={sibling distance=6em},
	  edge from parent/.style={draw, -latex},
	  local bounding box=n_dtree,
	  scale=1,transform shape,
	  every node/.style = {align=center, text centered, minimum height=2em},
	  ]

	  	\node[dtnode](r_epsi){$\epsilon$}
	  	child {node[dtleaf]{$q_0$} edge from parent node[left, near start]{$\top$}}
	  	child {node[dtnode](r_b){$b$}
	  		child {node[dtnode_temp](r_ab){$ab$}
	  			child {node[dtleaf](r_q1){$q_1$} edge from parent node[left, near start]{$\top$}}
	  			child {node[dtleaf](r_q2){$q_2$} edge from parent node[right, near start]{$\bot$}}
	  			edge from parent node[left, near start]{$\top$}}
	  		child {node[dtleaf](r_q3){$q_3$} edge from parent node[right, near start]{$\bot$}}
	  		edge from parent node[right, near start]{$\bot$}}
	  	;
	  
	  \end{scope}
	  \begin{pgfonlayer}{background}
	  \begin{scope}

	  	\node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(r_ab) (r_q1) (r_q2)](fit1){};
	     
	  \end{scope}
	  \end{pgfonlayer}
	  \end{tikzpicture}
	
	}
	\caption{The complete discrimination tree after split}\label{fig:marking_extract_3}
	\end{subfigure}
	\caption{An example of marking block and extract subtrees}\label{fig:marking_extract}
\end{figure}

\subsubsection{Extracting Subtrees}

After marking nodes and open transitions, for every output symbol, a subtree of the new discriminator can be made by extracting the elements marked with this symbol. As shown in Figure \ref{fig:marking_extract_2}, two subtrees are directly extracted.

A few further fixes are still needed. In case a inner node has only one child node (e.g. $ n_b $ in Figure \ref{fig:marking_extract_2}), this node is now redundant as a discriminator. Therefore it can be directly replace with its child node. And transitions pointing to this node are moved to its child node. Another case is when a inner node has now no child node. As it has no child marked with the same output, there must be an incoming transition in this node. Therefore this transition can be made tree-transition and a new state is also thereby made. And this node is now a leaf node associated with the new state.

The new discriminator is final, and the extracted inner nodes remain temporary. And a splitting iteration is so far accomplished. The result of our example after splitting is shown in Figure \ref{fig:marking_extract_3}. The finalization is not yet finished until no temporary nodes left.

\subsection{Hypothesis Stabilization}
In this step, \ttt\ checks the output consistency of the hypothesis. To achieve this, the discrimination tree is used reversely. As a key feature of the discrimination tree, every state can be discriminated by the root of the subtree that contains the corresponding leaf node. As shown in Algorithm \ref{alg:checkConsistency}, a state is checked by every subtree its corresponding node is in. The inconsistency found in process is raised as a counterexample, with which the hypothesis can be further refined. Once all states pass the check, the hypothesis is proved consistent with the discrimination tree.

\begin{algorithm}[h]
	\caption{Check Consistency of a State}\label{alg:checkConsistency}
	\begin{algorithmic}
		
		\Require Discrimination Tree $ \mathcal{DT} $, State $ s $
		\Ensure Inconsistency or null
		\State Let $ n $ be the leaf node in $ \mathcal{DT} $ that corresponds to $ s $
		\State Let $ p $ be $ n $
		\Repeat
			\State Let $ p $ be the parent of $ p $
			\State Let $ w_{dt} $ be the discriminator of $ p $
			\State Let $ o_{dt} $ be the parent edge label of $ n $
			\If{$ \lambda_{\mathcal{H}}(s, w_{dt}) \neq o_{dt} $}
				\State Let $ w_{as} $ be the unique access sequence of $ s $
				\State \Return $ w_{as} \cdot w_{dt} $ as an inconsistency
			\EndIf
		\Until{$ p $ is root of $ \mathcal{DT} $}
		\State \Return null
		
	\end{algorithmic}
\end{algorithm}

As the check requires only internal data, no membership queries are invoked. It is clear that parallelization would not change the complexity with regard to queries. Thus this step is not involved in the following parallelizing parts of this thesis.


% \section{Adaption to Mealy Machine}
\section{Summary: Sources of Membership Queries}\label{sec:sourcesMQ}
In this chapter a general work flow of \ttt\ is presented. More importantly, we pointed out the spots that membership queries are invoked. For clarity, these spots are summarized here.

\begin{description}
	\item[Hypothesis Construction] invokes 1 MQ by initialization.
	\item[Closing Transitions] invokes maximum $ depth_{max}(\mathcal{DT}) $ MQs for each open transitions, executed multiple times in every learning circle. The query complexity can be given by $ \mathcal{O}(kn^2)$, where $ k=|\varSigma| $ and $ n $ is the number of states.
	\item[Decomposition] invokes $ \lceil log_{2}(|eq|) \rceil $ MQs for each learning circle.
	\item[Marking Block] invokes MQs for each state (if not deducted with discrimination tree) and each incoming transition in the target temporary block (the number of transitions varies with the length of counterexample), executed probably several times in each learning circle. 
	% \item[Transition Creation] invokes 1 MQ per new transition for Mealy Machines
\end{description}


\begin{table}[h]
	\centering

	\begin{tabular}{l|cc|cc}
		\hline
		\hline
		Source & \multicolumn{2}{c|}{Query Cost} & \multicolumn{2}{c}{Symbol Cost} \\ \hline
		Initiation & 1 & 0.0014\% & 0 & 0\%\\
		Decomposition & 2198 & 3.0388\% & 22224 & 2.7982\%\\
		Marking Block & 581 & 0.8032\% & 6873 & 0.8654\%\\
		Close Transitions & 69552 & 96.1566\% & 765125 & 96.3364\% \\
		Total & 72332 && 794222 &\\
		\hline
		\hline
	\end{tabular}
	\caption{Portion of membership queries from different sources: Pots2 with white-box equivalence oracle}\label{tab:sources_mqs1}
	\begin{tabular}{l|cc|cc}
		\hline
		\hline
		Source & \multicolumn{2}{c|}{Query Cost} & \multicolumn{2}{c}{Symbol Cost} \\ \hline
		Initiation & 1 & 0.0014\% & 0 & 0\%\\
		Decomposition & 2273 & 3.1441\% & 31570 & 3.1020\%\\
		Marking Block & 2652 & 3.6684\% & 33473 & 3.2890\%\\
		Close Transitions & 67358 & 93.1723\% & 952496 & 93.5911\% \\
		Split State & 10 & 0.0138\%& 182 & 0.0179\% \\
		Total & 72294 && 1017721 &\\
		\hline
		\hline
	\end{tabular}
	\caption{Portion of membership queries from different sources: Pots2 with W-method equivalence oracle}\label{tab:sources_mqs2}
\end{table}

Table \ref{tab:sources_mqs1} shows an the membership queries form learning the pots2 (664 States, 32 Inputs), a realistic model describing a old telephony system \cite{garavel_cadp_2011}, with a white-box equivalence oracle, which means the counterexamples are short. With a more realistic setup (\ref{tab:sources_mqs2}), in which the equivalence oracle is simulated by means of invoking membership queries (with W-method Test \cite{chow_testing_1978, ipate_bounded_2010, ipate_w-method_2007}), the portion of Decomposition and Block Marking is increased. However, the membership queries for closing transitions still takes the most part. Due to the huge demand on space and time with simulated equivalence tests, the most experiments in this thesis are using the white-box equivalence oracle.