\chapter{Other Optimization}\label{cpt:otherParallelization}

In Chapter \ref{cpt:parallelization} and \ref{cpt:mod_parallelization}, the \ttt\ algorithm is parallelized with different strategies. In this chapter, we focus on the optimization of the cache oracle and the equivalence simulation with the power of parallelization.

\section{Efficiency improvement of Cache Oracle}\label{sec:fillupcache}
% Motivation: batch not full after cache. The computing power of the distributed oracle system is therefore not fully squeezed out. In order to make use of these idle worker, a sequence of query word are filled into the batch.

% choise of word sequence

% Note: cache oracle fails to be deployed for EQ by learning large automat (z.B. pots2) due to its storage requirement

\subsection{Motivation}
Normally a cache oracle is deployed between the membership oracle interface for the learner and invoking actual membership queries to prevent duplicated queries. That is to say, the cache oracle controls the actual batches that sent to the workers. It can be observed, that some batches have a number of queries that less than, or not a multiple of the number of workers. Therefore some workers are assigned more queries than the others, which is a waste of computing power. If some queries that can be useful in the future are assigned to these idle workers, the learning process can be further accelerated.

\subsection{Strategy}

The strategy for filling up the batches are rather simple here. Given the number of workers $ p $, after answered by the cache oracle, a batch can have
\begin{itemize}
	\item 0 membership queries, which requires nothing from the actual oracle and can be ignored;
	\item $ q $ queries, so that $ np<q<(n+1)p, n\in \mathbb{N} $, which can be filled up to $ (n+1)p $ queries; or
	\item $ np $ queries, in which $ n>0, n\in \mathbb{N} $, which can be sent to the actual oracle directly.
\end{itemize}
With a static parallel scheduler oracle, the parallel query complexity after filling does not increase. Due to additional answers in the cache, there may be less queries from the batch to send to the actual oracle, which may lead to a decrease of the parallel query complexity.

For minimally influence of the parallel symbol complexity, the injected words are supposed to be as short as possible. Therefore we generate lexicographic ordered series of $ \varSigma^* $ as the injected words. For example, the series of $ \varSigma=\{0,1\} $ is 
\begin{quotation}
	$ \epsilon, 0, 1, 00, 01, 10, 11, 000, \ldots $
\end{quotation}

Before a word is injected, it is checked in the cache and the current batch, which ensures no duplicated queries are sent to the actual oracle. Also, in most of the cases, the injected words are not longer than the words in the batch. However there are exceptions. Consider the situation of the first batch of the learning, the batch from the initiation would only consist of a word $ \epsilon $. In the filling stage, every injected word is longer than $ \epsilon $. So the parallel symbol complexity can be increased in rare case. 
\subsection{Evaluation}
To evaluate the influence of this strategy, we compared the complexity of learning the same DFA with normal cache oracle and with this fill-up cache oracle. it can be expected that the performance can be affected by the number of workers and the density of batches.
% 

\begin{figure}[h]
	\centering
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/fillup_worker_q.pdf}
		\caption{Parallel query speedup with normal parallel \ttt\ learner }
		\label{fig:fillup_worker1}
	\end{subfigure}
	\hspace{.02\textwidth}
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/fillup_worker_s.pdf}
		\caption{Parallel symbol speedup with normal parallel \ttt\ learner }
		\label{fig:fillup_worker2}
	\end{subfigure}

	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/fillup_adj_worker_q.pdf}
		\caption{Parallel query speedup with self-adjusting parallel \ttt\ learner }
		\label{fig:fillup_worker3}
	\end{subfigure}
	\hspace{.02\textwidth}
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/fillup_adj_worker_s.pdf}
		\caption{Parallel symbol speedup with self-adjusting parallel \ttt\ learner}
		\label{fig:fillup_worker4}
	\end{subfigure}
	\caption{The effect of filling batches in cache oracle: learning DFAs with 500 States and 32 Inputs, number of workers 4 to 64}\label{fig:fillup_worker}
\end{figure}

Figure \ref{fig:fillup_worker1} and \ref{fig:fillup_worker2} show the results of deploying the fill-up cache oracle. The performance by learning with more than 32 workers was improved around 20\%. And in Figure \ref{fig:fillup_worker3} and  \ref{fig:fillup_worker4}, the performance of learning with self-adjusting depth algorithm, which has been hardly accelerated with the normal cache oracle, was also improved by around 10\% by the case of 32 workers and more than 15\% by 64 workers.


\section{Parallelization of Equivalence Oracles}
% It is a small modification, but helps a lot on performance
% 饼图：query来源分布
\subsection{Motivation} \label{sec:paralleleq_motiv}
As presented in the last two chapters, parallelization of the \ttt\ Algorithm accelerated the learning process. However, as in a black box configuration, the current equivalence oracle also generates membership queries to simulate the equivalence test, whose amount takes the dominating proportion of all membership queries. E.g. by learning the repetitively mentioned example in \ref{sec:example_normal}, the equivalence oracle was simulated by invoking membership queries with W-method test, and the number of those is 6185. In comparison to the 117 queries from learner itself, this portion is huge. Further more, cost of testing target system with more states and inputs is much more expensive than this.
% to add: how big is the portion, how much faster will it be
Although this is not a part of the \ttt\ Algorithm, but the parallelization of equivalence test would greatly increase the performance of the learning simulation. 


\subsection{Strategy}
As shown in Algorithm \ref{alg:findCE}, a typical equivalence oracle generates a series of words, and compares the query results from membership oracles and those from processing in the hypothesis model given by learner. If every word within a specifically defined depth produces the same result in both the membership oracle and the hypothesis model, this model can be considered as equivalent to the SUL. Otherwise, the word is returned as counterexample. Note that this is not a full equivalence test, and a full equivalence here is theoretically impossible assuming that the SUL is a black box with unknown internal structure.
\begin{algorithm}[th]
	\caption{Finding Counter Example}\label{alg:findCE}
	\begin{algorithmic}
	\Require Hypothesis model, Input alphabet, depth limit, Membership oracle
	\Ensure Counterexample or Null
	\Loop
	\State find next query word within alphabet and limit
	\If{found}
		\State query this in oracle
		\State answer this with model
		\If{not equal}
			\State \textbf{return} Counterexample
		\EndIf
	\Else
		\State \textbf{return} Null
	\EndIf
	\EndLoop
	
	\end{algorithmic}

\end{algorithm}

The idea to parallelize this process is simple and similar to that of the \ttt\ algorithm: \textit{collect queries and send them in batches.} Instead of being directly queried, the words are firstly put into a temporary list, which will be afterwards queried in a batch by invoking  \lstinline[basicstyle=\ttfamily]|sulOracle.processQueries| from the membership oracle. After that, each word from the list will be processed by the hypothesis model. If the two answers are not equal, the method returns this query word as a counterexample. 

Hence a level of collecting, more specifically the size of a batch, must be defined. On one side, to obtain more benefit of parallelism, larger batches are required. On the other side, if one query word proved to be counterexample, no other queries are needed. Therefore large batches produce redundant queries.

A straightforward choice of batch size is the number of workers. In an simplified scenario, each query word will be sent to a different dedicated worker. Due to the word generation methods of equivalence oracle, these words are most possibly of similar length. Therefore the querying time will be reduced multiple times depending on the number of workers. Since the queries are simultaneously handled and the length of those are similar, the redundant queries will not influence the total query time.

\subsection{Outcome}
As mentioned in Section \ref{sec:paralleleq_motiv}, this modification will not affect the complexity of learning process. And due to the huge amount of membership queries invoked in simulating equivalence tests, the accelerate ratio is almost equal to the number of worker oracles. 

As this is not our main topic, only the example of small DFA (from Section \ref{sec:example_normal}) learning process is shown here. The equivalence tests cost 6685 queries and 40114 symbols. After parallelized with preferred batch size 8 (equals the number of worker oracles), the parallel query cost was reduced to 774, and the parallel symbol cost was reduced to 5609. The speedup was 7.9910 and 7.1517 respectively. For more complicated systems, the efficiency is often very close to 100\%.