\chapter{Parallelization of \ttt\ Algorithm}\label{cpt:normalParallelization}
\label{cpt:parallelization}
% Strategy to parallelzition
%  where queries are generated
%  closeTransition
%  prepareSplit
In the last chapter, interesting spots involving membership queries are pointed out. And the challenge of this chapter is approaching the parallelization.

The core and basic idea of parallelization is to collect as many independent membership queries as possible and send them to the parallel oracle in a batch. Note that the adaption made in this chapter is considered ``harmless'', as the parallelization neither requires more membership queries than the original version of \ttt\, nor saves any. Therefore, the query complexity and the symbol complexity should remain the same. 

First in this chapter, the details of parallelizing each part of the \ttt\ Algorithm will be presented. Then the parallelized \ttt\ Algorithm will be evaluated in different scenarios.
\section{Parallel Membership Oracle}

%TODO Add a figure
The parallelization of \ttt\ is based on the concept of \textit{the teachers' crowd}, which is, as presented in \cite{howar_teachers_2012} and previously introduced in Section \ref{sec:distributedMQ}, a parallel membership oracle realized with an array of distributed independent membership oracles over the same SUL ("workers"), and a scheduler oracle that assigns queries to them. 

% 作为并行的基础

There are two criteria to assign membership queries to worker oracles. One is the \textit{dynamic} criterion. Under this criterion the worker oracles are managed in a pool. The scheduler oracle assigns one membership query at a time if there is an available worker. The other criterion is \textit{static}. That is, the batch is divided most possibly equally by the scheduler, and assigned to the workers.

% query是如何分配的
For the purpose of simulation, we use the static criterion. The reason is that the experiment environment does not affect the results of parallelization with this criterion.

% simulstion 的实现

\section{Parallel Closing of Transitions }\label{sec:parallelCloseTransitions}
As described in \ref{sec:sourcesMQ}, most membership queries are invoked to close transitions. Thus the parallelization of this function has an essential position. 

\subsection{Strategy}
The process of close transitions is presented in \ref{alg:closeTransitions}. A SIFT operation is invoked for each open transition, and new states are created when needed. The membership queries are required in the SIFT operation.

Without adding complexity of the original \ttt\ Algorithm, the parallelization of the membership queries from one SIFT operation is impossible. The reason is that the query result of one step decides which route is chosen, hence the next query word depends on the result of last query. 

However, when more than one open transitions exist, they can be closed parallelly. There are several challenges to achieve this. First, to collect queries for one step of SIFTing all open transitions, the SIFT process must be interrupted, and the queries must be associated with the invoking transition, and so are the results. Second, during the SIFT process, there can be new state thus new open transitions created. Therefore, the new transitions should be SIFTed immediately in the same batch.

\begin{algorithm}[h]
	\caption{Parallel Transitions Closing}\label{alg:parallelCloseTransitions_normal}
	\begin{algorithmic}
		\Require List of Open Transitions $ \mathcal{L} $, Discrimination Tree, Membership Oracle $ oracle $, Soft Flag $ isHard $
		\Ensure Learner with all transitions closed
		
		\State Let $ Map_{query} $ be a Map(Transition $ \rightarrow $ Query)
		
		\Repeat
		
		\While{$ \mathcal{L} $ is not empty }
			\State Let Transition $ t $ be $ \mathcal{L}.pop()$ 
			\If{$ t $ is tree transition}
				\State continue
			\EndIf
			\State \Call{checkTarget}{$ Map_{query} $, $ t $}
		\EndWhile
		\State $ oracle.processQueries(Map_{query}.values) $ \Comment{The MQs are sent in a batch}

		\For{each transition $ t $ in $ Map_{query}.keys $}
			\State Let Node $ n $ be the non-tree target of $ t $
			\State $ n \leftarrow n.child(Map_{Query}[t].output)$
			\State $ t.setNonTreeTarget(n) $
			\State \Call{checkTarget}{$ Map_{query} $, $ t $}
		\EndFor
		\Until{$ Map_{query} $ is empty \textbf{\&} $ \mathcal{L} $ is empty}
	\algstore{parallelClosing}
		        
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}[h]
	\ContinuedFloat
	\begin{algorithmic}
	\algrestore{parallelClosing}
		\Function{checkTarget}{Map $ Map_{query} $, Transition $ t $}
		 	\State Let Node $ n $ be the non-tree target of $ t $
			\If{$ n $ is leaf node}
			\If{$ n $ has no associated state}
			\State Create State $ s $ according to $ t $
			\State Associate $ n $ with $ s $
			\State Initialize $ s $
			\State Remove $ t $ from $ Map_{query} $
			\EndIf
			\ElsIf{$! isHard $ \textbf{\&} $ n $ is not temp}
			\State Remove $ t $ from $ Map_{query} $
			\Else
			\State $ Map_{query} .put$($ t $, new Query($ t.accessSequence \cdot n.discriminator $))
			\EndIf
		\EndFunction
	\end{algorithmic}
\end{algorithm}

Algorithm \ref{alg:parallelCloseTransitions_normal} shows the function of parallelly closing transitions. The first problem is solved by introducing a map pairing each transition to a query object, which contains the query word and the result (after answered), and merging the SIFT operation inside our new process. We check the target node of each open transition for the ending condition of SIFT before adding it to the map. If it needs further SIFT operation, this transition is inserted to the map with respective created query object. This before querying loop ensures transitions in the open list to be added into the map, that includes the transitions created within this process. Thus the second problem is also solved.

After each open transition is processed, the queries in the map are sent to the oracle as a batch. With the query results, the targets of transitions in the map are set to the respective child nodes. Then the transition is either removed from the map if SIFT is finished, or assigned a new query object. This after querying loop ensures each transition in the map is updated.

These two loops are enclosed in an outer loop, which only ends when the map is cleared, which means all transitions are closed, and the open transition list is empty, which means there are no new transitions created in this iteration.



\subsection{Outcome}

As discussed in \ref{sec:stateInit}, transitions are created when new states are initialized. By each initiation of state, exactly $ k=|\varSigma| $ new transitions are created. 
So if we have a oracle with enough many workers, so that every batch can be seen as parallel query cost 1, the close transitions process can be accelerated by factor $ k $. Further more, if the batches are scheduled statically, the worker that assigned with longest queries determines the parallel cost. For a configuration with $ p $ worker oracles, a batch with $ k $ queries has a parallel query complexity $ \mathcal{O}(\lceil\frac{k}{p}\rceil) $. Thus the speedup would be $ \mathcal{O}(\frac{k}{\lceil\frac{k}{p}\rceil}) $.

However the length of a parallel closing transitions process, i.e. the number of batches in one process, is determined by the transition with longest SIFT. After each batch, some of the transitions can be already finished. Therefore a long tail of batches is formed. To overcome this drawback, some modifications of this algorithm are discussed in Section \ref{sec:mod_closeTransitions}.

\begin{table}[h]
	\centering
	\begin{tabular}{l|c|c|c}
		\multicolumn{4}{l}{Number of Worker Oracle: 4}\\
		\hline
		\hline

		 & \ttt\ & Parallel \ttt\ & Speedup \\ \hline
		Query Complexity & 69552 & 69552 & - \\
		Number of Batches & 69552 & 13228 & - \\
		Parallel Query Complexity & 69552 & 23314 & \textbf{2.9833}\\
		Symbol Complexity & 766125 & 766125 & - \\
		Parallel Symbol Complexity & 766125 & 262833 & \textbf{2.9149}\\
		\hline
		\hline

	\end{tabular}
	
	\vspace{0.5cm}

	\begin{tabular}{l|c|c|c}
		\multicolumn{4}{l}{Number of Worker Oracle: 32}\\
		\hline
		\hline

		& \ttt\ & Parallel \ttt\ & Speedup \\ \hline
		Query Complexity & 69552 & 69552 & - \\
		Number of Batches & 69552 & 13229 & - \\
		Parallel Query Complexity & 69552 & 13229 & \textbf{5.2575}\\
		Symbol Complexity & 766125 & 766125 & - \\
		Parallel Symbol Complexity & 766125 & 155843 & \textbf{4.9160}\\
		\hline
		\hline

	\end{tabular}
	

	\caption{Closing transitions complexity of parallel \ttt\ in comparison with original \ttt\ : Learning Pots2 (32 Inputs, 664 States) with white-box equivalence oracle}\label{tab:parallelClosing1}
\end{table}


Table \ref{tab:parallelClosing1} shows the results of learning the same example DFA (Pots2). The query and symbol complexity are not changed as predicted. Under a configuration of 4 worker oracles, the learning process is accelerated nearly 3 times. However, the parallel query complexity is still larger than the number of batches. That is to say, with some batches, at least one of the worker oracles need to process more than one queries. In the second configuration with 32 worker oracles, the parallel query complexity is equal to the number of batches. (Normally the maximum batch size is larger than the number of inputs.) The speedup hits its ceiling by around 5 times.

%Figure \ref{fig:closeTransitions_Pots2} shows a slice of batches sent by closing transitions process. Due to the specific property of that DFA, many transitions can be SIFTed in just one step. Still, a typical long tail pattern is clear. 
\section{Parallel Block Marking}
\subsection{Strategy}
The next process to parallelize is the block marking. Marking a block, which is represented by its root node, needs a subtree traversal to locate open transitions and assign them an output symbol.

To collect the membership queries for parallelization, the traversal and the marking are split into two loops. In the first loop, a traversal of the subtree is performed, where instead of directly sending of the query word, the query words are put into a map paired with the respective open transition. Due to the fact that no membership queries are needed in most of the cases for leaf nodes (in fact we never observed a single case for that, but it is yet not proven), those steps are kept unchanged in the parallelized version.

Once the queries are all collected, they are sent to the membership oracle in a batch. With the output of the queries, the transitions are marked as the original version of \ttt\ .

\subsection{Outcome}
Each parallel block marking generates exactly one batch of queries. And the number of open transitions are associated with $ k $. Therefore the theoretical optimal speedup would be $ k $. However, the open transitions left for marking are normally less in practice depending on the structure of target automaton.


\section{Experimentation}
\subsection{Example: Learning a small DFA}\label{sec:example_normal}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{bilder/example_dfa.pdf}
	\caption{DFA example with 10 states, 3 inputs}\label{fig:dfa_example}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{bilder/example_normal_bar.pdf}
	\caption{Size of batches: learning a DFA example with 10 states, 3 inputs}\label{fig:normalBatchSize}
\end{figure}

In order to explain the parallel learning process more clearly, a small DFA example is first presented. The target DFA has 10 states and 3 inputs (shown in Figure \ref{fig:dfa_example}). And the parallel membership oracle has 8 worker oracles.

The learning process took 7 iteration of equivalence tests, and 49 batches of membership queries were invoked, which consisted of 1 batch from initiation (1 query), 3 from marking blocks (12 queries), 15 from counterexample decomposition (15 queries), and 30 from closing transitions (89 queries). The parallel query cost was 50. Figure \ref{fig:normalBatchSize} shows the size of the batches. The batches from decomposition were not parallelized, therefore each batch has only one query. A majority of batches from closing transitions had a size of 3 queries, which was the number of inputs($ k $). This agrees with the prediction.

In Figure \ref{fig:exp_closetransitions}, one of the closing transitions process - in the fifth learning iteration, after $ \textit{\textsc{Refine}}_{\textit{\textsc{Single}}} $ and before finalizing discriminators - is shown. This process generated 3 batches, each had 3 queries (Batch 31, 32, 33 in Figure \ref{fig:normalBatchSize}).

There were 3 open transitions after the new state $ s7 $ being created. At the beginning of closing, they were pointing to the root node of the discrimination tree(\ref{fig:exp_closetransitions1}). In the first iteration of closing transitions, one query was asked for each open transition, i.e. $ 2 0 | \epsilon $, $ 2 1 | \epsilon $, and $ 2 2 | \epsilon $, as $ s7 $ had a access sequence $ 2 $. The answers of those happened to be all $ \bot $. Thus, after updating the transitions, the status was as \ref{fig:exp_closetransitions2}.
As these transitions were still not pointing to leaves or temporary nodes, they were continued to be SIFTed down in iteration 2 (\ref{fig:exp_closetransitions3}), and a batch of 3 queries were generated.
In the third iteration, after one more layer down for each transitions, the closing process are finished (\ref{fig:exp_closetransitions4}).

The transitions in this closing process had coincidentally the same steps, so that the query speedup for this process is exactly $ k=3 $. Considering that $ k $ is usually larger in realistic scenarios, the parallelization was successful. However, the problem of this parallel transitions closing process has been already visible: despite 8 worker oracles there were, each batch had only 3 queries. The computing power was wasted.
\begin{figure}[h]


	\begin{subfigure}[t]{\textwidth}
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{scope}[
			level/.style={sibling distance=8em},
			level distance=5em,
			level 1/.style={sibling distance=13em},
			level 2/.style={sibling distance=6.5em},
			level 3/.style={sibling distance=3.25em},
			level 4/.style={sibling distance=3.25em},
			every node/.style = {align=center, text centered, minimum height=2em},
			edge from parent/.style={draw,-latex},
			local bounding box=dtree,
			scale=0.6,transform shape
			]
			\node[dtnode](epsilon){$\epsilon$}
			child {node[dtnode](n0){$0$} 
				child {node[dtleaf](s4){$ s4 $} edge from parent node [left,near start] {$\top$}}
				child {node[dtnode](n10){$ 10 $} 
					child {node[dtleaf](s5){$s5$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s1){$s1$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[left,near start] {$\top$}}
			child {node[dtnode](lab){$0$}
				child {node[dtnode](l00){$00$} 
					child {node[dtleaf](s3){$s3$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s0){$s0$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[left, near start]{$\top$}}
				child {node[dtnode](lb){$2$}
					child {node[dtleaf, fill=white](s2){$s2$} edge from parent node[left, near start]{$\top$}}
					child {node[dtnode_temp](l3){$100$}
						child {node[dtleaf](s6){$s6$} edge from parent node[left, near start]{$\top$}}
						child {node[dtleaf](s7){$s7$} edge from parent node[right, near start]{$\bot$}}
						edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[right,near start] {$\bot$}
			}
			;
			\node[circle, dashed, draw, above right=1.5em and 3.5em of epsilon](ss7){$s7$};
			\path[->]
			(ss7) edge[bend right=30, dashed]  node [auto, inner sep=0pt] {$0,1,2$} (epsilon)
			;
			
			\end{scope}
		  \begin{pgfonlayer}{background}
		  \begin{scope}
		  
		  \node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(l3) (s6) (s7)](fit1){};
		  
		  \end{scope}
		  \end{pgfonlayer}
			\end{tikzpicture}
			\caption{Before closing transitions}\label{fig:exp_closetransitions1}
		\end{subfigure}
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{scope}[
			level/.style={sibling distance=8em},
			level distance=5em,
			level 1/.style={sibling distance=13em},
			level 2/.style={sibling distance=6.5em},
			level 3/.style={sibling distance=3.25em},
			level 4/.style={sibling distance=3.25em},
			every node/.style = {align=center, text centered, minimum height=2em},
			edge from parent/.style={draw,-latex},
			local bounding box=dtree,
			scale=0.6,transform shape
			]
			\node[dtnode](epsilon){$\epsilon$}
			child {node[dtnode](n0){$0$} 
				child {node[dtleaf](s4){$ s4 $} edge from parent node [left,near start] {$\top$}}
				child {node[dtnode](n10){$ 10 $} 
					child {node[dtleaf](s5){$s5$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s1){$s1$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[left,near start] {$\top$}}
			child {node[dtnode](nr0){$0$}
				child {node[dtnode](l00){$00$} 
					child {node[dtleaf](s3){$s3$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s0){$s0$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[left, near start]{$\top$}}
				child {node[dtnode](lb){$2$}
					child {node[dtleaf, fill=white](s2){$s2$} edge from parent node[left, near start]{$\top$}}
					child {node[dtnode_temp](l3){$100$}
						child {node[dtleaf](s6){$s6$} edge from parent node[left, near start]{$\top$}}
						child {node[dtleaf](s7){$s7$} edge from parent node[right, near start]{$\bot$}}
						edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[right,near start] {$\bot$}
			}
			;
			\node[circle, dashed, draw, above right=2.5em and 2.5em of nr0](ss7){$s7$};
			\path[->]
			(ss7) edge[bend right=30, dashed]  node [auto, inner sep=0pt] {$0,1,2$} (nr0)
			;
			
			\end{scope}
		  \begin{pgfonlayer}{background}
		  \begin{scope}
		  
		  \node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(l3) (s6) (s7)](fit1){};
		  
		  \end{scope}
		  \end{pgfonlayer}
			\end{tikzpicture}
			\caption{After first iteration of closing}
			\label{fig:exp_closetransitions2}
		\end{subfigure}
		\vspace{2mm}
	\end{subfigure}

	\begin{subfigure}[b]{\textwidth}
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{scope}[
			level/.style={sibling distance=8em},
			level distance=5em,
			level 1/.style={sibling distance=13em},
			level 2/.style={sibling distance=6.5em},
			level 3/.style={sibling distance=3.25em},
			level 4/.style={sibling distance=3.25em},
			every node/.style = {align=center, text centered, minimum height=2em},
			edge from parent/.style={draw,-latex},
			local bounding box=dtree,
			scale=0.6,transform shape
			]
			\node[dtnode](epsilon){$\epsilon$}
			child {node[dtnode](n0){$0$} 
				child {node[dtleaf](s4){$ s4 $} edge from parent node [left,near start] {$\top$}}
				child {node[dtnode](n10){$ 10 $} 
					child {node[dtleaf](s5){$s5$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s1){$s1$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[left,near start] {$\top$}}
			child {node[dtnode](lab){$0$}
				child {node[dtnode](l00){$00$} 
					child {node[dtleaf](s3){$s3$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s0){$s0$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[left, near start]{$\top$}}
				child {node[dtnode](n2){$2$}
					child {node[dtleaf, fill=white](s2){$s2$} edge from parent node[left, near start]{$\top$}}
					child {node[dtnode_temp](l3){$100$}
						child {node[dtleaf](s6){$s6$} edge from parent node[left, near start]{$\top$}}
						child {node[dtleaf](s7){$s7$} edge from parent node[right, near start]{$\bot$}}
						edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[right,near start] {$\bot$}
			}
			;
			\node[circle, dashed, draw, above right=2.5em and 1.5em of n2](ss7){$s7$};
			\path[->]
			(ss7) edge[bend right=10, dashed]  node [auto, inner sep=0pt] {$0,1$} (n2)
			(ss7) edge[bend right=10, dashed]  node [left, near end] {$2$} (l00)
			;
			
			\end{scope}
		  \begin{pgfonlayer}{background}
		  \begin{scope}
		  
		  \node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(l3) (s6) (s7)](fit1){};
		  
		  \end{scope}
		  \end{pgfonlayer}
			\end{tikzpicture}
			\caption{After second iteration of closing}
			\label{fig:exp_closetransitions3}
		\end{subfigure}
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\begin{tikzpicture}
			\begin{scope}[
			level/.style={sibling distance=8em},
			level distance=5em,
			level 1/.style={sibling distance=13em},
			level 2/.style={sibling distance=6.5em},
			level 3/.style={sibling distance=3.25em},
			level 4/.style={sibling distance=3.25em},
			every node/.style = {align=center, text centered, minimum height=2em},
			edge from parent/.style={draw,-latex},
			local bounding box=dtree,
			scale=0.6,transform shape
			]
			\node[dtnode](epsilon){$\epsilon$}
			child {node[dtnode](n0){$0$} 
				child {node[dtleaf](s4){$ s4 $} edge from parent node [left,near start] {$\top$}}
				child {node[dtnode](n10){$ 10 $} 
					child {node[dtleaf](s5){$s5$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s1){$s1$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[left,near start] {$\top$}}
			child {node[dtnode](lab){$0$}
				child {node[dtnode](l00){$00$} 
					child {node[dtleaf](s3){$s3$} edge from parent node[left, near start]{$\top$}}
					child {node[dtleaf](s0){$s0$} edge from parent node[right, near start]{$\bot$}}
					edge from parent node[left, near start]{$\top$}}
				child {node[dtnode](lb){$2$}
					child {node[dtleaf,fill=white](s2){$s2$} edge from parent node[left, near start]{$\top$}}
					child {node[dtnode_temp](l3){$100$}
						child {node[dtleaf](s6){$s6$} edge from parent node[left, near start]{$\top$}}
						child {node[dtleaf](s7){$s7$} edge from parent node[right, near start]{$\bot$}}
						edge from parent node[right, near start]{$\bot$}}
					edge from parent node[right, near start]{$\bot$}}
				edge from parent node[right,near start] {$\bot$}
			}
			;
			\node[circle, dashed, draw, above right=2.5em and 1.5em of l3](ss7){$s7$};
			\path[->]
			(ss7) edge[bend right=10, dashed]  node [left] {$2$} (s3)
			(ss7) edge[bend right=10, dashed]  node [auto, inner sep=0pt, near start] {$0,1$} (l3)
			;
			
			\end{scope}
		  \begin{pgfonlayer}{background}
		  \begin{scope}
		  
		  \node[fill=gray!20, inner sep=3pt , densely dashed, rounded corners, thin, rectangle, fit=(l3) (s6) (s7)](fit1){};
		  
		  \end{scope}
		  \end{pgfonlayer}
			\end{tikzpicture}
			\caption{Finished after third iteration of closing}
			\label{fig:exp_closetransitions4}
		\end{subfigure}
	\end{subfigure}
\caption{An example of parallel transitions closing}\label{fig:exp_closetransitions}
\end{figure}


\subsection{Learning a Realistic Automaton}
The example of \lstinline[basicstyle=\ttfamily]|Pots2| DFA shown in Section \ref{sec:sourcesMQ} was first learned again with the parallel \ttt\ algorithm. As mentioned above, the query and symbol complexity remained the same. Table \ref{tab:parallel_pots2} shows the results. Due to the specific property of this DFA, many transitions pointed directly to a rejected state, for which the SIFT process ended in one step. As a result, the long tail effect was significant. Aside from that, the decomposition process was not parallelized. So the speedup for the whole learning process was not optimal.

\begin{table}[h]
	\centering
	
	\begin{tabular}{l|c|c|c}
		\multicolumn{4}{l}{Parallel Query Cost:}\\
		\hline
		\hline
		Source & \ttt\ & Parallel \ttt\ & Speedup \\ \hline
		Initiation & 1 & 1 & 1.0000 \\
		Decomposition & 2198& 2198 & 1.0000\\
		Marking Block & 581 & 92 & 6.3152 \\
		Close Transitions & 69552 & 13229 & 5.2575\\\hline
		Total & 72332 & 15520 & \textbf{4.6606} \\
		\hline
		\hline
	\end{tabular}

		\vspace{0.5cm}
	\begin{tabular}{l|c|c|c}
		\multicolumn{4}{l}{Parallel Symbol Cost:}\\
		\hline
		\hline
		Source & \ttt\ & Parallel \ttt\ & Speedup \\ \hline
		Initiation & 0 & 0 & -\\
		Decomposition & 22224 &22224 & 1.0000\\
		Marking Block & 6873 & 1220 & 5.6636\\
		Closing Transitions   & 765125 & 155843 & 4.9160\\\hline
		Total & 794222 & 179288 & \textbf{4.4299} \\
		\hline
		\hline
	\end{tabular}
	\caption{Results of learning \lstinline[basicstyle=\ttfamily]|Pots2| with parallel \ttt\ (32 worker oracles)}\label{tab:parallel_pots2}
\end{table}

To show a more general result, we use the parallel \ttt\ learner to learn some randomly generated DFAs.

\subsection{Learning Randomly Generated Automata}

\subsubsection{Impact of the Number of States}
Figure \ref{fig:normalSpeedupStates} shows results of the first Set of experiments. To investigate the influence of the number of inputs to the speedup, we generated 5 random DFAs with each number of states from 50 to 500, and 32 inputs. In each experiment, the speedup was acquired by comparing the query/symbol cost to the parallel cost. 

It can be observed that the speedup with random DFA was significant larger than that with \lstinline[basicstyle=\ttfamily]|Pots2|, and was closer to the predicted value. That shows again the particularity of the \lstinline[basicstyle=\ttfamily]|Pots2| DFA. On the other hand, it shows a weak point of random DFA: the situation in practice can be very different from that in laboratory.

The query speedup is slightly lower than the ratio with symbol. The reason is that during the learning process, the discrimination tree and the query word length grows. The speedup is actually an average value. And the symbol speedup, in most cases, can be seen as the query one weighted with the symbol length of each query from this point of view. Normally, with larger discrimination trees, the parallelization performs better, where the weight is also larger. Which explains the difference between Figure \ref{fig:normalSpeedupStates_1} and \ref{fig:normalSpeedupStates_2}.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.9\textwidth}
		\includegraphics[width=\textwidth]{bilder/speedupNormalStates_q.pdf}
		\caption{Speedup of query cost}
		\label{fig:normalSpeedupStates_1}
	\end{subfigure}
		\begin{subfigure}{.9\textwidth}
			\includegraphics[width=\textwidth]{bilder/speedupNormalStates_s.pdf}
			\caption{Speedup of symbol cost}
			\label{fig:normalSpeedupStates_2}
		\end{subfigure}
	\caption{Learning DFA with different number of states: 32 inputs, 50 to 500 states, 32 worker oracles}\label{fig:normalSpeedupStates}
\end{figure}



\subsubsection{Impact of the Number of Worker Oracles}
Figure \ref{fig:normalSpeedupWorkers} shows results of the second set of experiments. In this set, the target DFAs were with the same number of states (500) and inputs (32), and the number of worker oracles varied from 2 to 64. For each number of worker oracles, 30 examples were learned.

The figure shows an obvious stair-step shape. The reason is that the queries in a batch can not evenly assigned to workers, and the worker with largest assignment determines the parallel cost. For example, with 4 worker oracles, a batch of size 5 has the same parallel query cost as a batch of size 8. Further more, the majority of membership queries come from the closing transitions process, which is associated with the number of inputs $ k $ (32 in this experiment). Thus a line $ f(x)=\frac{k}{\lceil\frac{k}{x}\rceil} $ is drawn in the figure, which fits well to the results. Also, the line $ f(x)=x $ indicates the upper limit of speedup.

It can be observed that with the number of worker oracles less than 9, the speedup as well as the stair step line is rather linear and close to the upper limit. With increased workers, the stair step shape is more obvious. On each stair step, the ratio increases only slowly. Note that when $ n_{workes} \ge k $, the theoretical ratio remains $ k $. This stair step line can be useful to choose the proper number of workers for a certain target setup.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/speedupNormalWorkers_q.pdf}
		\caption{Speedup of query cost}
		\label{fig:normalSpeedupWorkers_1}
	\end{subfigure}
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{bilder/speedupNormalWorkers_s.pdf}
		\caption{Speedup of symbol cost}
		\label{fig:normalSpeedupWorkers_2}
	\end{subfigure}
	\caption{Learning DFA with different number of worker oracles: 32 inputs, 500 states, 2 to 64 worker oracles}\label{fig:normalSpeedupWorkers}
\end{figure}

% \section{Discussion: The Impact of Number of Workers}

\subsubsection{Variable Number of Inputs}
Figure \ref{fig:normalSpeedupInputs} shows results of the third set of experiments. In this set, the target DFAs were with the same number of states (200) and inputs varies from 4 to 512, and the number of worker oracles were set as 32. For each number of inputs, 5 examples were learned.

\begin{figure}[ht]
	\centering
	\begin{subfigure}{.9\textwidth}
		\includegraphics[width=\textwidth]{bilder/speedupNormalInputs_q.pdf}
		\caption{Speedup of query cost}
		\label{fig:normalSpeedupInputs_1}
	\end{subfigure}
	\begin{subfigure}{.9\textwidth}
		\includegraphics[width=\textwidth]{bilder/speedupNormalInputs_s.pdf}
		\caption{Speedup of symbol cost}
		\label{fig:normalSpeedupInputs_2}
	\end{subfigure}
	\caption{Learning DFA with different number of inputs: 4 to 512 inputs, 200 states, 32 worker oracles}\label{fig:normalSpeedupInputs}
\end{figure}

The same theoretical ratio line $ f(k)=\frac{k}{\lceil\frac{k}{p}\rceil} $ is drawn in these figures. As the argument changes from $ p $ to $ k $, the shape is transformed. With a large number of inputs, the speedup is very close to the limit. In other words, the performance is limited by the number of workers. Thus, increasing the worker oracles would further accelerate the learning process.