\chapter{Introduction} 

%TODO introduction 

% Applications of learning DFA
Learning automata is a problem with wide applicability, e.g. in bio-informatics \cite{misra_lacas:_2009}, signal processing \cite{hashim_application_1986, nambiar_genetic_1992}, automation \cite{unsal_multiple_1999}, software engineering \cite{granmo_learning_2007} etc. Especially in the field of model-checking and model-based testing, automata learning shows its great competence \cite{hutchison_introduction_2011, choi_guided_2013, mao_learning_2011} since Steffen \textit{et al}. introduced automata learning in the field of formal methods in their work \cite{hagerer_efficient_????,hagerer_model_2002,steffen_behavior-based_2003, hungar_test-based_2003}. 

The automata learning algorithm has developed for nearly 30 years. In the year of 1987, Dana Angluin established an active automata learning framework and the learning algorithm $ L^* $ in her seminal work \textit{Learning regular sets from queries and counterexamples} \cite{angluin_learning_1987}. The model can be inferred by asking two kinds of questions, i.e. the \textit{membership queries} and the \textit{equivalence queries}. 
Upon this \textit{minimally adequate teacher} framework, more \textit{active} learning algorithms have been developed. e.g. \textit{Rivest and Schapire} \cite{rivest_inference_1993} introducing the analysis of the counterexamples, \textit{Kearns and Vazirani} \cite{kearns_introduction_1994} introducing the discrimination tree, \textit{Observation Packs} \cite{balcazar_algorithms_1997, falk_howar_zulu_2010,balle_implementing_2010,howar_active_2012} that combining the discrimination tree and the counterexample analysis, \ttt\ \cite{isberner_ttt_2014, howar_tutorial:_2014, isberner_foundations_2015} eliminating the redundancy in the \textit{Observation Packs} etc. There are also \textit{passive} approaches \cite{oncina_identifying_1992, dupont_regular_1994, murphy_passively_1995, lucas_learning_2005, heule_exact_2010} that replace the teacher with a set of samples.

On the other hand, as though parallel computing has been rapidly evolved and becomes much more affordable nowadays, the parallelism is applied to automata learning in only a few cases. 
In his work \textit{Exact learning of formulas in parallel}  \cite{bshouty_exact_1992, bshouty_exact_1997}, Bshouty proved the lower and upper bounds of exactly learning boolean formulas in parallel, which gives an overview of how the parallelism can improve the performance by learning DFA.
The same results were also obtained independently by Balcázar \textit{et al.}. \cite{balcazar_note_1993}
Further more, in their work \textit{An Optimal Parallel Algorithm for Learning DFA} \cite{balcazar_optimal_1994}, Balcázar \textit{et al.} extended Angluin's Algorithm \cite{angluin_learning_1987} with Parallelism to get a time optimal learning algorithm, which achieved the above mentioned upper bound. This optimal time complexity, however, was achieved at the cost of low parallel efficiency.
Another approach of parallel automata learning was made by Akram \textit{et al.} with PSMA (Parallel State Merging Algorithm), presented in their work \textit{PSMA: A parallel algorithm for learning regular languages} \cite{akram_psma:_2010}. The PSMA is a passive learning algorithm, which uses a fixed set of samples instead of the teachers.
Howar \textit{et al.} adapted the MAT framework to parallelism as a crowd of teachers \cite{howar_teachers_2012}. In this paper, a parallelized version of $ L^*_M $ \cite{hutchison_introduction_2011} was presented. And the evaluation section indicates also parallelized version of the $ DHC $ (\textit{Direct Hypothesis Construction}) algorithm \cite{merten_automata_2012}, and the \textit{Observation Pack} Algorithm \cite{falk_howar_zulu_2010}. The \ttt\ algorithm, which proved to have better sequential performance, still lacks parallelization.


%Among those algorithms, \ttt\ learning algorithm provides outstanding performance with regard to query and symbol complexity. Therefore, we expect further improvement of performance by applying parallelism to the \ttt\ learning algorithm.

% Definition of the problem
\section{Research Problems}
This thesis focuses on the parallelization of the \ttt\ learning algorithm. More specifically, the main questions addressed in this thesis can be described as: 
\begin{quotation}
	\textsl{Develop strategies for parallelizing the \ttt\ algorithm and evaluate their implementations.}
	
\end{quotation}
This problem can be resolved into following parts:
\begin{enumerate}
	\item Analyzing \ttt\ algorithm to address interesting spots that potentially can be parallelized.
	\item Finding and implementing strategies of parallelization.
	\item Evaluating these implementations.
\end{enumerate}


\section{Related Work}
This thesis is based on the \ttt\ learning algorithm, a redundancy-free learning algorithm for DFA and Mealy Machines. This algorithm was developed by Isberner and presented in his work \textit{The TTT algorithm: A redundancy-free approach to active automata learning.} \cite{isberner_ttt_2014}. In his PhD thesis \textit{Foundations of active automata learning: an algorithmic perspective} \cite{isberner_foundations_2015}, the algorithm was further refined. \ttt\ uses discrimination tree as its core data structure and introduces some unique features to reduce redundant query and symbol costs, including \textit{discriminator finalization} that eliminates the symbol redundancy in membership queries, and 
\textit{consistency checking} that reduces the number of equivalence queries.

Another foundation of this thesis is the adapted MAT framework introduced by Howar \textit{et al.} in their work \textit{The teachers’ crowd: The impact of distributed oracles on active automata learning} \cite{howar_teachers_2012}, which introduced the concept of the parallel membership oracle.

Both the \ttt\ learning algorithm and the parallel membership oracle were implemented in the \textit{LearnLib} \cite{isberner_open-source_2015}, a library of tools for automata learning developed with Java. As a parallel modification of the \ttt\ , the algorithms described in this thesis are also implemented in \textit{LearnLib}. 



\section{Outline}
This thesis is organized as follows:
In Chapter \ref{cpt:activeLearning}, some fundamental concepts are introduced. This includes the active automata learning framework that both \ttt\ and parallel \ttt\ are built upon, the idea of parallelization, and how the parallelized learner can be evaluated. 
Before parallelizing it, the \ttt\ algorithm is first briefly introduced in Chapter \ref{cpt:ttt}, during which the interesting spots for parallelization are highlighted. 

In Chapter \ref{cpt:parallelization}, a basic strategy of parallelizing \ttt\ is presented. This approach is more of a parallelization add-on to the original \ttt\ learner, without changing its complexity. And the result is evaluated with different experiments.
Chapter \ref{cpt:mod_parallelization} takes the parallelization further. By means of adding some redundant queries, some modifications are made to the parallel algorithm. An improved performance can be expected in evaluation.

Chapter \ref{cpt:otherParallelization} presents the efforts to optimize the learning process with parallelization, including a modified cache oracle that generate queries for idle workers and parallelized equivalence oracles sending membership queries in batches.

At last Chapter \ref{cpt:conclusion} concludes this thesis and lists some possible work for the future.