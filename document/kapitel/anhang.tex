% anhang.tex
\chapter{The Experiment Tool}

This part presents the tools developed for the experimentation in this thesis.

\section{Experiment Utilities}
This part of the tools is developed in the Learnlib framework.
\subsection{Query Counter}
For recording the behavior of every parallel worker oracle, a more detailed statistic tool is demanded. 

The \lstinline[basicstyle=\ttfamily]|QueryCounter| class extends a list of \lstinline[basicstyle=\ttfamily]|QueryRecord|, which presents a batch of queries, and stores its source, timestamp, query count, symbol count. In some cases parallel query and symbol count are also stored in the records. The source is automatically acquired by invoking stack tracing methods.

More than storing the statistics, the counter provides methods to get the parallel query and symbol count for each batch using the timestamp. In section \ref{sec:oraclechain} it is shown that, each worker oracle has its own counter, as well as the scheduler oracle. To get the parallel cost, the batches from the worker oracles are first mapped to that from the scheduler. For each batch from scheduler - it is the batch sent by learner - the longest query and symbol cost are selected as its parallel cost.

\subsection{Oracle Chain}\label{sec:oraclechain}
To trace the behavior of different oracles without changing its implementation, a statistic oracle is introduced. It does nothing but recording and passing the batches to the next oracle. With this extra layer of oracle, as well as the parallel worker oracles, the creation of oracles for each experiment is quite troublesome. Therefore an abstract chain of oracle is introduced.

\begin{figure}[th]
	\centering
	\tikzstyle{oracle}=[draw, very thick, rounded corners, text width=10em, text centered, minimum height=1.8em]
	\tikzstyle{dot}=[text width=5em, text centered, minimum height=2.5em]
	\tikzstyle{moracle}=[draw, very thick, rounded corners, text width=4em, text centered, minimum height=2.5em]

	  \begin{tikzpicture}[node distance=4cm, auto,>=latex', thick]
	  \node[oracle] at (10em,13em)(top){\textbf{Statistic Oracle \\ (Top)}};
	  \path[->] node[oracle] at (10em,9em)(fork){Fork Oracle}
	  (top) edge node {} (fork);
	  
	  \path[->] node[oracle] at (1em,6em) (cacheOracle){Cache Oracle} (fork) edge node {} (cacheOracle);
	  	  
	  \path[->] node[oracle] at (1em,2em) (afterCache){\textbf{Statistic Oracle \\ (After Cache)}} 	  (cacheOracle) edge node {} (afterCache);
	  

	  \path[->] node[oracle] at (1em,-2em) (parallelac){Parallel Oracle} 	  (afterCache) edge node {} (parallelac);
	  
	  \path[->] node[oracle] at (21em,6em) (parallelOracle){ Parallel Oracle}(fork) edge node {} (parallelOracle);
	  
	  \path[->] node[moracle] at (15em,2em) (worker1){\textbf{Statistic Oracle}}(parallelOracle) edge node {} (worker1);
	  \path[->] node[moracle] at (15em,-2em) (worker1sul){SUL}(worker1) edge node {} (worker1sul);
	  
	  \path[->] node[moracle] at (20em,2em) (worker2){\textbf{Statistic Oracle}}(parallelOracle) edge node {} (worker2);
	  \path[->] node[moracle] at (20em,-2em) (worker2sul){SUL}(worker2) edge node {} (worker2sul);
	  
	  \path[->] node[dot] at (24em,2em) (parallelDots){ \dots};
	  \path[->] node[dot] at (24em,-2em) (parallelDots2){ \dots};
	  
	  \path[->] node[moracle] at (28em,2em) (worker3){\textbf{Statistic Oracle}}(parallelOracle) edge node {} (worker3);
	  \path[->] node[moracle] at (28em,-2em) (worker3sul){SUL}(worker3) edge node {} (worker3sul);
	  
	  
	  \path[->] node[moracle] at (-5em,-6em) (worker1ac){\textbf{Statistic Oracle}}(parallelac) edge node {} (worker1ac);
	  \path[->] node[moracle] at (-5em,-10em) (worker1sulac){SUL}(worker1ac) edge node {} (worker1sulac);
	  
	  \path[->] node[moracle] at (0em,-6em) (worker2ac){\textbf{Statistic Oracle}}(parallelac) edge node {} (worker2ac);
	  \path[->] node[moracle] at (0em,-10em) (worker2sulac){SUL}(worker2ac) edge node {} (worker2sulac);
	  
	  \path[->] node[dot] at (4em,-6em) (parallelDotsac){ \dots};
	  \path[->] node[dot] at (4em,-10em) (parallelDotsac2){ \dots};
	  
	  \path[->] node[moracle] at (8em,-6em) (worker3ac){\textbf{Statistic Oracle}}(parallelac) edge node {} (worker3ac);
	  \path[->] node[moracle] at (8em,-10em) (worker3sulac){SUL}(worker3ac) edge node {} (worker3sulac);
	  
	  
%	  \path[->] node[moracle] at (-3em,0) (mOracle1){\small Membership Oracle}
%	  (parallelOracle) edge node {}(mOracle1);
%	  \path[->] node[moracle] at (-0.5em,0) (mOracle2){\small Membership Oracle}
%	  (parallelOracle) edge node {}(mOracle2);
%	  \path[->] node[dot] at (1.25em,0) (mOracledot){\small \dots};
%	  
%	  \path[->] node[moracle] at (3em,0) (mOracle3){\small Membership Oracle}
%	  (parallelOracle) edge node {}(mOracle3);

	  \end{tikzpicture}
	
	
	\caption{Oracle chain}
	\label{fig:oraclechain}
\end{figure}

As shown in Figure \ref{fig:oraclechain}, the oracle chain is invoked by passing batches to the \textit{top} oracle. After recording the original batch information, the batch is cloned into two batches. One is passed directly to parallel oracle, while the other is further optimized in cache oracle. The optimized batch is recorded in \textit{after cache} oracle, and than passed to a different parallel oracle. To get the accurate parallel cost, each SUL has its own counter.

As the structure is fixed, the creation of a oracle chain requires only the number of workers. And the parallel behavior during the learning process - with and without cache oracle at the same time - is detailed recorded.

\subsection{Statistic Experiment}

A \lstinline[basicstyle=\ttfamily]|StatisticExperiment| object is introduced to wrap the learner, oracles, run the learning loop, and get the statistics. 

The process of creating an experiment is thereby simplified to: \textbf{1.} specify the number of worker oracles and create the \lstinline[basicstyle=\ttfamily]|StatisticExperiment| object, \textbf{2.} create and assign a learner, and \textbf{3.} create and assign an equivalence oracle.
The running of an experiment is simplified to only invoking \lstinline[basicstyle=\ttfamily]|StatisticExperiment.run()| after creation. And the statistic values are directly available.
 
\section{Post-Processing}
To generate clear and interactive chart from the statistic values, we chose to use Highcharts JS \footnote{Highcharts is a charting library written in pure JavaScript, free of charge for personal and non-profit use under Creative Commons Attribution-NonCommercial 3.0 License. See http://www.
	highcharts.com/ for details} and python as post processing tool. The work flow is as follows: 

\subsection{Recording}
After experiment, write the statistics to data files in format according to the protocol. A simple writer is also implemented. Users have option to record a single experiment batch for batch, or use the learner comparator to run a set of experiments and record only the essential data.

\subsection{Chart Generator}

With the data file, the python script can generate different kind of charts up to the choice of users. The charts are output to an HTML file, or files. The charts are integrated with animation, colored legend and mouse hover tooltips. Figure \ref{fig:screenshotBatchsize} shows a generated chart for the learning results in Section \ref{sec:example_adj}. The two charts present the size of batches in the learning, with and without membership queries generated by equivalence oracle. Note that for clarity, queries from the last equivalence test are hidden, otherwise the chart will be unreadable. Different sources are labeled with colors, cached queries in a batch are also annotated. 

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{bilder/screenshot-crop.pdf}
	\caption{HTML file example generated by post-processor}\label{fig:screenshotBatchsize}
\end{figure}

